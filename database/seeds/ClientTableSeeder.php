<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'name' => 'Bruno',
            'lastname' => 'Fereira da Silva',
            'email' => 'bruno.silvatba@gmail.com',
            'cpf' => '14106017733',            
            'password' => bcrypt('123456'),
        ]);
    }
}
