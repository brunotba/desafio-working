<?php 
return [
	'titulo' => 'Clients',
	'editar' => 'edit',
	'label_buscar' => 'Search by',
	'label_nome' => 'Name',
	'acoes' => 'Actions:',
	'itens' => 'Items per page',
	'novo' => 'New',

	'menu' => [
		'cliente' => 'Client',
		'listar_cliente' => 'List customers',
		'novo_cliente' => 'New customer',
		'produto' => 'Product',
		'listar_produto' => 'List products',
		'vender' => 'Sell',
		'comprar' => 'Purchase',
		'negociacao' => 'Negotiations',
		'minhas_vendas' => 'My sales',
		'minhas_compras' => 'My shopping',
	],
];