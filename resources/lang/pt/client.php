<?php 
return [
	'titulo' => 'Clientes',
	'editar' => 'editar',
	'label_buscar' => 'Buscar por',
	'label_nome' => 'Nome',
	'acoes' => 'Ações:',
	'itens' => 'Itens por página',
	'novo' => 'Novo',

	'menu' => [
		'cliente' => 'Cliente',
		'listar_cliente' => 'Listar clientes',
		'novo_cliente' => 'Novo cliente',
		'produto' => 'Product',
		'listar_produto' => 'Listar produtos',
		'vender' => 'Vender',
		'comprar' => 'Comprar',
		'negociacao' => 'Negociações',
		'minhas_vendas' => 'Minhas vendas',
		'minhas_compras' => 'Minhas compras',
	],
];