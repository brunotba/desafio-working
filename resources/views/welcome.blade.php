@extends('layouts.system')

@section('content')
<!--banner-->    
<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Dashboard</span>
    </h2>
</div>
<!--//banner-->
<!--content-->
<div class="content-top">
    <div class="col-md-4 ">
        <div class="content-top-1">
            <div class="col-md-6 top-content">
                <h5>Produtos</h5>
                <label>8761</label>
            </div>
            <div class="col-md-6 top-content1">       
                <div id="demo-pie-1" class="pie-title-center" data-percent="25"> <span class="pie-value"></span> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="col-md-4 ">
        <div class="content-top-1">
            <div class="col-md-6 top-content">
                <h5>Vendas</h5>
                <label>6295</label>
            </div>
            <div class="col-md-6 top-content1">       
                <div id="demo-pie-2" class="pie-title-center" data-percent="50"> <span class="pie-value"></span> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="col-md-4 ">
        <div class="content-top-1">
            <div class="col-md-6 top-content">
                <h5>Clientes</h5>
                <label>3401</label>
            </div>
            <div class="col-md-6 top-content1">       
                <div id="demo-pie-3" class="pie-title-center" data-percent="75"> <span class="pie-value"></span> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>

<div class="content-top">
    <div class="col-md-4 ">
        <div class="content-top-1">
            <div class="col-md-6 top-content">
                <h5>Pedidos</h5>
                <label>8761</label>
            </div>
            <div class="col-md-6 top-content1">       
                <div id="demo-pie-4" class="pie-title-center" data-percent="25"> <span class="pie-value"></span> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="col-md-4 ">
        <div class="content-top-1">
            <div class="col-md-6 top-content">
                <h5>Vendas</h5>
                <label>6295</label>
            </div>
            <div class="col-md-6 top-content1">       
                <div id="demo-pie-5" class="pie-title-center" data-percent="50"> <span class="pie-value"></span> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="col-md-4 ">
        <div class="content-top-1">
            <div class="col-md-6 top-content">
                <h5>Cards</h5>
                <label>3401</label>
            </div>
            <div class="col-md-6 top-content1">       
                <div id="demo-pie-6" class="pie-title-center" data-percent="75"> <span class="pie-value"></span> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>

<div class="copy">
    <p> &copy; {{date('Y')}} Working Minds. Todos direitos reservados | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
</div>
</div>
@endsection






@section('js')
<!-- pie-chart- -->
<script src="{{url('js/pie-chart.js')}}" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#demo-pie-1').pieChart({
            barColor: '#3bb2d0',
            trackColor: '#eee',
            lineCap: 'round',
            lineWidth: 8,
            onStep: function (from, to, percent) {
                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
            }
        });

        $('#demo-pie-2').pieChart({
            barColor: '#fbb03b',
            trackColor: '#eee',
            lineCap: 'butt',
            lineWidth: 8,
            onStep: function (from, to, percent) {
                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
            }
        });

        $('#demo-pie-3').pieChart({
            barColor: '#ed6498',
            trackColor: '#eee',
            lineCap: 'square',
            lineWidth: 8,
            onStep: function (from, to, percent) {
                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
            }
        });

        $('#demo-pie-4').pieChart({
            barColor: '#c2c2c2',
            trackColor: '#eee',
            lineCap: 'square',
            lineWidth: 8,
            onStep: function (from, to, percent) {
                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
            }
        });

        $('#demo-pie-5').pieChart({
            barColor: '#FD2036',
            trackColor: '#eee',
            lineCap: 'square',
            lineWidth: 8,
            onStep: function (from, to, percent) {
                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
            }
        });

        $('#demo-pie-6').pieChart({
            barColor: '#EECC00',
            trackColor: '#eee',
            lineCap: 'square',
            lineWidth: 8,
            onStep: function (from, to, percent) {
                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
            }
        });

    });

</script>

@endsection