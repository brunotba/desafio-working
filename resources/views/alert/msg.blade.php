@if(session()->has('msg'))
<div class="alert alert-{{session()->get('type')}}">
	<p class="text-msg">{{session()->get('msg')}}</p>
</div>
@endif