@extends('layouts.system')

@section('content')

<div class="banner">
	<div class="row">
		<div class="col col-md-4">
			<h2 style="padding: 5.7px 0;">
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<a href="{{route('list-client')}}">Cliente</a>
				<i class="fa fa-angle-right"></i>
				<span>{{isset($data->id_client) ? 'Editar' : 'Novo'}}</span>
			</h2>
		</div>
	</div>
</div>

<div class="grid-system">
	@include('alert.msg')

	<form method="post" action="{{route('save-update-client', isset($data->id_client) ? [$data->id_client] : false)}}">
		@csrf
		<div class="horz-grid">			
			<div class="row">
				<div class="col col-md-12">
					<h3 class="title" class="">Clientes</h3>						
				</div>	
			</div>

			<div class="row">
				<div class="col col-md-4">
					<div class="form-group {{$errors->has('nome') ? 'has-error' : ''}}">
						<label class="control-label" for="nome">Nome</label>
						<input value='{{old('nome', $data->name)}}' name="nome" type="text" class="form-control" id="nome">
						@if($errors->has('nome'))
						<label class="text-error">{{$errors->first('nome')}}</label>
						@endif
					</div>
				</div>
				<div class="col col-md-4">
					<div class="form-group {{$errors->has('sobrenome') ? 'has-error' : ''}}">
						<label class="control-label" for="sobrenome">Sobrenome</label>
						<input value='{{old('sobrenome', $data->lastname)}}' name="sobrenome" type="text" class="form-control" id="sobrenome">
						@if($errors->has('sobrenome'))
						<label class="text-error">{{$errors->first('sobrenome')}}</label>
						@endif
					</div>
				</div>
				<div class="col col-md-4">
					<div class="form-group {{$errors->has('cpf') ? 'has-error' : ''}}">
						<label class="control-label" for="cpf">CPF</label>
						<input type="text" value='{{maskCPF(old('cpf', $data->cpf))}}' name="cpf" class="form-control" id="cpf">
						@if($errors->has('cpf'))
						<label class="text-error">{{$errors->first('cpf')}}</label>
						@endif
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col col-md-4">
					<div class="form-group {{$errors->has('sobrenome') ? 'has-error' : ''}}">
						<label class="control-label" for="email">E-mail</label>
						<input name="email" value='{{old('email', $data->email)}}' type="email" class="form-control" id="email">
						@if($errors->has('email'))
						<label class="text-error">{{$errors->first('email')}}</label>
						@endif
					</div>
				</div>

				<div class="col col-md-4">
					<div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
						<label class="control-label" for="senha">Senha</label>
						<input name="password" type="password" class="form-control" id="senha">
						@if($errors->has('password'))
						<label class="text-error">{{$errors->first('password')}}</label>
						@endif
					</div>
				</div>

				<div class="col col-md-4">
					<div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
						<label class="control-label" for="password_confirmation">Confirmar senha</label>
						<input name="password_confirmation" type="password" class="form-control" id="password_confirmation">
						@if($errors->has('password_confirmation'))
						<label class="text-error">{{$errors->first('password_confirmation')}}</label>
						@endif
					</div>
				</div>
				<div class="col col-md-12">
					<button class="btn btn-danger" type="submit">Salvar</button>
					<a href="{{route('list-client')}}" class="btn btn-default pull-right" type="submit">Cancelar</a>
				</div>

			</div>
		</div>
	</form>	
</div>

@endsection
