@extends('layouts.system')

@section('content')
<form action="{{route('list-client')}}" method="get">
	@csrf
	<div class="banner">

		<div class="row">
			<div class="col col-md-4">
				<h2 style="padding: 5.7px 0;">
					<a href="{{url('/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
					<span>Clientes</span>
				</h2>
			</div>
			
			<div class="col col-md-2 col-md-offset-5">
				<div class="form-group">				
					<input name="search_for" value="{{isset($search_for) ? $search_for : ''}}" placeholder="Buscar por" type="text" class="form-control" id="buscar_por">
				</div>
			</div>	

			<div class="col col-md-1">
				<a class="pull-right btn btn-small btn-primary" href="{{route('new-client')}}">Novo</a>
			</div>
		</div>
	</div>

	<div class="grid-system">
		@include('alert.msg')
		<div class="horz-grid">
			<div class="row">
				<div class="col col-md-12">
					<h3 class="title" class="">Clientes</h3>
					
					<div class="table-responsive">
						<table id='table-index' class="table  table-striped  hover-table  ">
							<thead>
								<tr>
									<th style="width: 1px"></th>
									<th style="width: 1px"><a href="{{route('list-client')}}?{{generate_link($results->getAppends(),'orderByColumn','id_client')}}">#</a></th>
									<th><a href="{{route('list-client')}}?{{generate_link($results->getAppends(),'orderByColumn','name')}}">Nome</a></th>
									<th><a href="{{route('list-client')}}?{{generate_link($results->getAppends(),'orderByColumn','cpf')}}">CPF</a></th>
									<th><a href="{{route('list-client')}}?{{generate_link($results->getAppends(),'orderByColumn','email')}}">E-mail</a></th>
									<th colspan="2"></th>
								</tr>
							</thead>

							<tbody>
								@forelse($results as $result)
								<tr>
									<td><input type="checkbox" name="id_client[]" value='{{$result->id_client}}'></td>
									<td>{{$result->id_client}}</td>
									<td>{{$result->name . ' ' . $result->lastname}}</td>
									<td>{{maskCPF($result->cpf)}}</td>
									<td>{{$result->email}}</td>
									<td style="width: 1px" ><a href="{{route('edit-client',[$result->id_client])}}"><i class="fa fa-edit"></i></a></td>
									<td style="width: 1px"><a href="{{route('delete-client',[$result->id_client])}}"><i class="fa fa-trash"></i></a></td>
								</tr>
								@empty
								<tr>
									<th colspan="5">
										<p class="text-center">Não existem dados para serem exibidos.</p>
									</th>
								</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
				<div class="col col-md-7">
					{{$results->links()}}				
				</div>
				<div class="col col-md-3">
					<small>Ações: </small>
					<div class="form-group" id="marker_remover">	
						<select  class="form-control pull-right" name="marker_remover">
							<option value='0'>Selecione..</option>						
							<option value="1">Remover marcados</option>						
						</select>									
					</div>
				</div>
				<div class="col col-md-2">
					<small>Itens por página: </small>
					<div class="form-group" id="per_page">	
						<select  class="form-control pull-right" name="perPage">
							<option {{$perPage == 5 ? 'selected' : ''}} >5</option>
							<option {{$perPage == 10 ? 'selected' : ''}} >10</option>
							<option {{$perPage == 25 ? 'selected' : ''}} >25</option>
							<option {{$perPage == 50 ? 'selected' : ''}} >50</option>
							<option {{$perPage == 75 ? 'selected' : ''}} >75</option>
							<option {{$perPage == 100 ? 'selected' : ''}} >100</option>
						</select>									
					</div>
				</div>	
				
			</div>
		</div>
	</div>
</form>
@endsection

@section('js')
<script>
	$('#per_page').change(function(){
		$('form').submit(); 
	});

	$('#marker_remover').change(function(){
		var com = confirm('Realmente deseja remover os registros marcados?');
		if(com){
			var url = '{{route('delete-client')}}';

			$('form').attr('method', 'POST').attr('action', url).submit(); 
			return 1;
		}
		$(this).find('option[value=0]').attr('selected', 'selected');
	});

	$('tr').click(function(){
		$(this).find('input').trigger('click');
	})
</script>
@endsection