<!DOCTYPE HTML>
<html>
<head>
    <title>Working Minds</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="{{url('/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <link href="{{url('css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{url('css/font-awesome.css')}}" rel="stylesheet"> 

    <!-- Custom and plugin javascript -->
    <link href="{{url('css/custom.css')}}" rel="stylesheet">
    @yield('css')
</head>
<body>
    <div id="wrapper">

        @include('includes.nav')

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="content-main">
                @yield('content')
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <script src="{{url('js/jquery.min.js')}}"> </script>
    <!-- Mainly scripts -->
    <script src="{{url('js/jquery.metisMenu.js')}}"></script>
    <script src="{{url('js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{url('js/custom.js')}}"></script>
    <script src="{{url('js/screenfull.js')}}"></script>
    <script>
        $(function () {
            $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

            if (!screenfull.enabled) {
                return false;
            }

            $('#toggle').click(function () {
                screenfull.toggle($('#container')[0]);
            });            
            
        });
    </script>

    <!--skycons-icons-->
    <script src="{{url('js/skycons.js')}}"></script>
    <!--//skycons-icons-->
    <!---->
    <!--scrolling js-->
    <script src="{{url('/js/jquery.nicescroll.js')}}"></script>
    <script src="{{url('/js/scripts.js')}}"></script>
    <!--//scrolling js-->
    <script src="{{url('/js/bootstrap.min.js')}}"></script>

    @yield('js')
</body>
</html>

