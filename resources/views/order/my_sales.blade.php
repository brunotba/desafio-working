@extends('layouts.system')

@section('content')

<form action="{{route(Route::currentRouteName())}}" method="get">
	@csrf
	<div class="banner">

		<div class="row">
			<div class="col col-md-4">
				<h2 style="padding: 5.7px 0;">
					<a href="{{url('/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
					<span>Minhas vendas</span>
				</h2>
			</div>
			
			<div class="col col-md-2 col-md-offset-6">
				<div class="form-group">				
					<input name="search_for" value="{{isset($search_for) ? $search_for : ''}}" placeholder="Buscar por" type="text" class="form-control" id="buscar_por">
				</div>
			</div>			
		</div>
	</div>

	<div class="grid-system">
		@include('alert.msg')
		<div class="horz-grid">
			<div class="row">
				<div class="col col-md-12">
					<h3 class="title" class="">Minhas vendas</h3>
					
					<div class="table-responsive">
						<table id='table-index' class="table  table-striped  hover-table  ">
							<thead>
								<tr>
									<th>Nome do produto</th>
									<th style="width: 1px">Qtd</th>
									<th>Valor unitário</th>
									<th>Valor total</th>
									<th>Data</th>
									<th>Comprador</th>
									<th></th>
								</tr>
							</thead>

							<tbody>
								@forelse($results as $result)
								<tr>
									<td>{{$result->product->name_product}}</td>
									<td class="text-center">{{$result->quantity}}</td>
									<td>R$ {{\number_format($result->product->unitary_value, 2, ',','.')}}</td>
									<td>R$ {{\number_format($result->amount(), 2, ',','.')}}</td>
									<td>{{date('d/m/Y',strtotime($result->created_at))}}</td>
									<td>{{$result->buyer->name}}</td>
									<td>
										<div class="group-form">
											<select data-idorder='{{$result->id_order}}' class="alter-status">
												@foreach($allStatus as $key => $status)
												<option {{$result->status == $key ? 'selected' : ''}} value='{{$key}}'>{{$status}}</option>
												@endforeach
											</select>
										</div>										
									</td>									
								</tr>
								@empty
								<tr>
									<th colspan="6">
										<p class="text-center">Não existem dados para serem exibidos.</p>
									</th>
								</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
				<div class="col col-md-7">
					{{$results->links()}}				
				</div>
				<div class="col col-md-3">
					@if(Route::currentRouteName() == 'list-product')
					<small>Ações: </small>
					<div class="form-group" id="marker_remover">	
						<select  class="form-control pull-right" name="marker_remover">
							<option value='0'>Selecione..</option>						
							<option value="1">Remover marcados</option>						
						</select>									
					</div>
					@endif
				</div>	
				<div class="col col-md-2">
					<small>Itens por página : </small>
					<div class="form-group" id="per_page">	
						<select class="form-control pull-right" name="perPage">
							<option {{$perPage == 5 ? 'selected' : ''}} >5</option>
							<option {{$perPage == 10 ? 'selected' : ''}} >10</option>
							<option {{$perPage == 25 ? 'selected' : ''}} >25</option>
							<option {{$perPage == 50 ? 'selected' : ''}} >50</option>
							<option {{$perPage == 75 ? 'selected' : ''}} >75</option>
							<option {{$perPage == 100 ? 'selected' : ''}} >100</option>
						</select>									
					</div>
				</div>	
				
			</div>
		</div>
	</div>
</form>
@endsection

@section('js')
<script>
	$('#per_page').change(function(){
		$('form').submit(); 
	});

	$('.alter-status').change(function(){
		var id_order = $(this).data('idorder');
		var status = $(this).val();
		$.ajax({
			url: '{{url('order/alter-status')}}/' + id_order + '/' + status,
			type: 'get'
		},'JSON').
		done(function(data){
			if(data.retorno == true){
				alert('Alterado com sucesso');
			}else{
				alert('Aconteceu algo de errado');
			}
		})
	});
</script>
@endsection