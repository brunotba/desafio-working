@extends('layouts.system')

@section('content')

<form action="{{route(Route::currentRouteName())}}" method="get">
	@csrf
	<div class="banner">

		<div class="row">
			<div class="col col-md-4">
				<h2 style="padding: 5.7px 0;">
					<a href="{{url('/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
					<span>Produtos</span>
				</h2>
			</div>
			
			<div class="col col-md-2 col-md-offset-5">
				<div class="form-group">				
					<input name="search_for" value="{{isset($search_for) ? $search_for : ''}}" placeholder="Buscar por" type="text" class="form-control" id="buscar_por">
				</div>
			</div>	

			<div class="col col-md-1">
				<a class="pull-right btn btn-small btn-primary" href="{{route('new-product')}}">Novo</a>
			</div>
		</div>
	</div>
	@if(Route::currentRouteName() != 'list-product')
</form>
@endif

<div class="grid-system">
	@include('alert.msg')
	<div class="horz-grid">
		<div class="row">
			<div class="col col-md-12">
				<h3 class="title">Produtos</h3>

				<div class="table-responsive">
					<table id='table-index' class="table  table-striped  hover-table  ">
						<thead>
							<tr>
								@if(Route::currentRouteName() == 'list-product')
								<th style="width: 1px"></th>									
								<th style="width: 1px"><a href="{{route(Route::currentRouteName())}}?{{generate_link($results->getAppends(),'orderByColumn','id_product')}}">#</a></th>
								@endif
								<th><a href="{{route(Route::currentRouteName())}}?{{generate_link($results->getAppends(),'orderByColumn','name_product')}}">Nome do produto</a></th>
								<th><a href="{{route(Route::currentRouteName())}}?{{generate_link($results->getAppends(),'orderByColumn','unitary_value')}}">Preço (R$)</a></th>	
								<th><a href="{{route(Route::currentRouteName())}}?{{generate_link($results->getAppends(),'orderByColumn','bar_code')}}">Cód. barra</a></th>	
								@if(Route::currentRouteName() != 'list-product')
								<th width="1%" style="width: 1px">Qtd</th>
								@endif								
								<th colspan="2"></th>
							</tr>
						</thead>

						<tbody>
							@forelse($results as $result)
							<tr>
								@if(Route::currentRouteName() == 'list-product')
								<td><input type="checkbox" name="id_product[]" value='{{$result->id_product}}'></td>									
								<td>{{$result->id_product}}</td>
								@endif
								<td>{{$result->name_product}}</td>
								<td>R$ {{number_format($result->unitary_value, 2, ',','.')}}</td>		
								<td>{{$result->bar_code}}</td>		
								@if(Route::currentRouteName() != 'list-product')
								<td>
									@cannot('update-product', $result)
									<div class="group-control">
										<input style="width:40px" class="input-control text-center" type="number" value="1" name="">
									</div>
									@endcannot
								</td>
								@endif								
								<td style="width: 1px">
									@can('update-product', $result)
									<a href="{{route('edit-product',[$result->id_product])}}"><i class="fa fa-edit"></i></a>
									@endcan

									@cannot('update-product', $result)
									<a title="Comprar" href=""><i data-idproduct='{{$result->id_product}}' class="fa buy fa-shopping-cart"></i></a>
									@endcannot
								</td>
								@if(Route::currentRouteName() == 'list-product')
								<td style="width: 1px">										
									<a href="{{route('delete-product',[$result->id_product])}}"><i class="fa fa-trash"></i></a>										
								</td>
								@endif
							</tr>
							@empty
							<tr>
								<th colspan="6">
									<p class="text-center">Não existem dados para serem exibidos.</p>
								</th>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
			<div class="col col-md-7">
				{{$results->links()}}				
			</div>
			<div class="col col-md-3">
				@if(Route::currentRouteName() == 'list-product')
				<small>Ações: </small>
				<div class="form-group" id="marker_remover">	
					<select  class="form-control pull-right" name="marker_remover">
						<option value='0'>Selecione..</option>						
						<option value="1">Remover marcados</option>						
					</select>									
				</div>
				@endif
			</div>	
			<div class="col col-md-2">
				<small>Itens por página : </small>
				<div class="form-group" id="per_page">	
					<select  class="form-control pull-right" name="perPage">
						<option {{$perPage == 5 ? 'selected' : ''}} >5</option>
						<option {{$perPage == 10 ? 'selected' : ''}} >10</option>
						<option {{$perPage == 25 ? 'selected' : ''}} >25</option>
						<option {{$perPage == 50 ? 'selected' : ''}} >50</option>
						<option {{$perPage == 75 ? 'selected' : ''}} >75</option>
						<option {{$perPage == 100 ? 'selected' : ''}} >100</option>
					</select>									
				</div>
			</div>	

		</div>
	</div>
</div>
@if(Route::currentRouteName() == 'list-product')
</form>
@endif
@endsection

@section('js')
<script>
	$('#per_page').change(function(){
		$('form').submit(); 
	});

	$('#marker_remover').change(function(){
		var com = confirm('Realmente deseja remover os registros marcados?');
		if(com){
			var url = '{{route('delete-product')}}';

			$('form').attr('method', 'POST').attr('action', url).submit(); 
			return 1;
		}
		$(this).find('option[value=0]').attr('selected', 'selected');
	});

	$('tr').click(function(){
		$(this).find('input').trigger('click');
	})

	$('.buy').click(function(){
		var qtd_itens = $(this).parents('tr').find('input[type=number]').val();
		var id_product = $(this).data('idproduct');
		url = '{{url('order/buy')}}/' + qtd_itens + '/' +id_product;
		window.location = url;
		return false;
	})
</script>
@endsection