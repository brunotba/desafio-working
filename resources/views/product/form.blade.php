@extends('layouts.system')

@section('content')

<div class="banner">
	<div class="row">
		<div class="col col-md-4">
			<h2 style="padding: 5.7px 0;">
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<a href="{{route('list-product')}}">Produto</a>
				<i class="fa fa-angle-right"></i>
				<span>{{isset($data->id_product) ? 'Editar' : 'Novo'}}</span>
			</h2>
		</div>
	</div>
</div>

<div class="grid-system">
	@include('alert.msg')

	<form method="post" action="{{route('save-update-product', isset($data->id_product) ? [$data->id_product] : false)}}">
		@csrf
		<div class="horz-grid">			
			<div class="row">
				<div class="col col-md-12">
					<h3 class="title" class="">Produto</h3>						
				</div>	
			</div>

			<div class="row">
				<div class="col col-md-4">
					<div class="form-group {{$errors->has('name_product') ? 'has-error' : ''}}">
						<label class="control-label" for="name_product">Nome do produto</label>
						<input value='{{old('name_product', $data->name_product)}}' name="name_product" type="text" class="form-control" id="name_product">
						@if($errors->has('name_product'))
						<label class="text-error">{{$errors->first('name_product')}}</label>
						@endif
					</div>
				</div>
				<div class="col col-md-4">
					<div class="form-group {{$errors->has('unitary_value') ? 'has-error' : ''}}">
						<label class="control-label" for="unitary_value">Valor (R$)</label>
						<input value='{{number_format(old('unitary_value', $data->unitary_value),2,',','.')}}' name="unitary_value" type="text" class="form-control" id="unitary_value">
						@if($errors->has('unitary_value'))
						<label class="text-error">{{$errors->first('unitary_value')}}</label>
						@endif
					</div>
				</div>				
			</div>
			<div class="row">
				<div class="col col-md-12">
					<button class="btn btn-danger" type="submit">Salvar</button>
					<a href="{{route('list-product')}}" class="btn btn-default pull-right" type="submit">Cancelar</a>
				</div>
			</div>
		</div>
	</form>	
</div>

@endsection
