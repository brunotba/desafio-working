<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Client;
use App\Order;
class OrderController extends Controller
{
	/*Lisda de pedidos*/
	public function index(Request $request)
	{   
		$perPage = $request->input('perPage', 10);
		$search_for = $request->input('search_for');

		$data = Order::orderBy('id_order', 'DESC');

		if($search_for){
			$data = $data->whereHas('product', function($q) use ($search_for) {
				$q->where('name_product','like','%'.$search_for.'%');
			});
		}

		$data = $data->where('client_id', auth()->user()->id_client);
		$data = $data->paginate($perPage);       

		$data->appends(['perPage' => $perPage]);                       
		$data->appends(['search_for' => $search_for]);

		return view('product.my_shopping')->with('results',$data)->with('perPage', $perPage)->with('search_for', $search_for);
	}

	public function my_sales(Request $request)
	{   
		$perPage = $request->input('perPage', 10);
		$search_for = $request->input('search_for');
		
		$status = new Order;
		$status = $status->getAllStatus();

		$data = Order::orderBy('id_order', 'DESC');
		$data = $data->whereHas('product', function($q) {
			$q->where('client_id', auth()->user()->id_client);
		});
		
		if($search_for){
			$data = $data->whereHas('product', function($q) use ($search_for) {
				$q->where('name_product','like','%'.$search_for.'%');
			});
		}

		$data = $data->paginate($perPage);       

		$data->appends(['perPage' => $perPage]);                       
		$data->appends(['search_for' => $search_for]);                       


		return view('order.my_sales')->with('allStatus', $status )->with('results',$data)->with('perPage', $perPage)->with('search_for', $search_for);
	}


	public function buy_product(Request $request)
    {
        $order = new Order;
        $order->product_id = $request->id_product;
        $order->client_id = auth()->user()->id_client;
        $order->quantity = $request->qty;
        $order->save();

        $msg = 'Erro em realziar a pedido.';
        $type = 'danger';

        if($order->id_order){
            $msg = 'Pedido realizado com sucesso.';
            $type = 'success';            
        }

        session()->flash('msg', $msg);
        session()->flash('type', $type);

        return redirect()->route('my-shopping');
    }

}
