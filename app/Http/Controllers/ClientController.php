<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation;
use Illuminate\Validation\Rule;
use App\Providers\AuthServiceProvider;
use Validator;

class ClientController extends Controller
{



    public function index(Request $request)
    {   

        $perPage = $request->input('perPage', 10);
        $search_for = $request->input('search_for');
        $orderByColumn = $request->input('orderByColumn', 'id_client');
        $orderBy = $request->input('orderBy', 'DESC');
        $orderByReverse = $orderBy == 'DESC' ? 'ASC' : 'DESC';

        $data = Client::orderBy($orderByColumn, $orderBy);
        
        if($search_for){
            $data = $data->where(function($q) use ($search_for){
                $q->where('name','like','%'.$search_for.'%');
                $q->orWhere('lastname','like','%'.$search_for.'%');
                $q->orWhere('cpf','like','%'.$search_for.'%');
                $q->orWhere('email','like','%'.$search_for.'%');
            });
        }

        $data = $data->paginate($perPage);       

        $data->appends(['perPage' => $perPage]);        
        $data->appends(['orderByColumn' => $orderByColumn]);        
        $data->appends(['orderBy' => $orderByReverse]); 

       // dd(generate_link($data->getAppends(),'orderBy','teste'));

        //dd($data->getAppendsLink());
        return view('client.index')->with('results',$data)->with('perPage', $perPage)->with('search_for', $search_for);
    }

    public function create()
    {   
        $data = new Client;
        return view('client.form')->with('data', $data);
    }

    public function store(Request $request, $id_client = false)
    {   
        $client = Client::firstOrNew(['id_client' => $id_client]);

        //removendo pontos e traços do CPF
        $request->request->add(['cpf' => $this->replaceCPF($request->cpf)]);

        $validator = Validator::make($request->all(), $this->rules($request, $client), $this->messages());

        $redirectErros = $id_client ? route('edit-client',[$id_client]) : route('new-client');

        if ($validator->fails()) {            
            return redirect($redirectErros)
            ->withErrors($validator)
            ->withInput();
        }
        
        $client->name = $request->nome;
        $client->lastname = $request->sobrenome;
        $client->email = $request->email;
        $client->cpf = $request->cpf;
        $client->password = bcrypt($request->password);
        $id_client ? $client->update() : $client->save();

        $msg = 'Erro ao salvar dados';
        $type = 'danger';

        if ($client) {
            $msg = 'Salvo com sucesso.';
            $type = 'success';
        }

        session()->flash('msg', $msg);
        session()->flash('type', $type);

        return redirect()->route('list-client');
    }

    public function edit(Client $client)
    {  
        return view('client.form')
        ->with('data', $client);
    }  

    public function destroy(Request $request)
    {
        $ids = is_array($request->id_client) ? $request->id_client : [$request->id_client];        
        $client = Client::whereIn('id_client', $ids)->delete();

        $msg = 'Erro ao remover registro';
        $type = 'danger';

        if ($client) {
            $msg = 'Registro(s) removido(s) com sucesso.';
            $type = 'success';
        }

        session()->flash('msg', $msg);
        session()->flash('type', $type);

        return redirect()->route('list-client');
    }

    public function rules($request, $client)
    {
        $validate = [
            'nome' => 'required|max:15',
            'sobrenome' => 'required|max:40',                        
            'email' => ['required', Rule::unique('clients')->ignore($client->id_client, 'id_client')],
            'cpf' => ['max:11','required', Rule::unique('clients')->ignore($client->id_client,'id_client')],
        ];

        if ($client->id_client == false || $request->password || $request->password_confirmation) {
            $validate['password'] = 'required|confirmed|min:6';
            $validate['password_confirmation'] = 'required|min:6';
        }

        return $validate;
    }

    public function messages()
    {
        return [
            'nome.required' => 'Campo :attribute obrigatório',
            'sobrenome.required' => 'Campo :attribute obrigatório',
            'email.required' => 'Campo :attribute obrigatório',
            'cpf.required' => 'Campo :attribute obrigatório',
            'nome.max' => 'O limite máximo de :max caracteres foi atigido',
            'sobrenome.max' => 'O limite máximo de :max caracteres foi atigido',          
            'cpf.max' => 'O limite máximo de :max caracteres foi atigido',
            'password.min' => 'Digite pelo menos :min caracteres',            
            'password.confirmed' => 'Senhas digitadas não são identicas',
            'password.required' => 'Campo :attribute obrigatório',
            'password_confirmation.required' => 'Campo confirmar senha obrigatório',

            'cpf.unique' => 'CPF já cadastrado no sistema.',
            'email.unique' => 'Email já cadastrado no sistema.',

        ];
    }

    public function replaceCPF($cpf)
    {
        return str_replace(['.','-'],'', $cpf);
    }

    public function authenticate(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {        
            return redirect(url('/'));
        }
        
        return redirect(url('/login'))->withErrors([
           'email' => 'confira seus dados e tente novamente.',          
        ])
        ->withInput();
    }

    public function logout()
    {
        Auth::logout();
        return redirect(url('/login'));
    }
}
