<?php

namespace App\Http\Controllers;

use App\Product;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation;
use Illuminate\Validation\Rule;
use Validator;
use App\Providers\AuthServiceProvider;
use Gate;
use App\Client;

class ProductController extends Controller
{
    public function index(Request $request, Client $client)
    {   

        $perPage = $request->input('perPage', 10);
        $search_for = $request->input('search_for');
        $orderByColumn = $request->input('orderByColumn', 'id_product');
        $orderBy = $request->input('orderBy', 'DESC');
        $orderByReverse = $orderBy == 'DESC' ? 'ASC' : 'DESC';

        $data = Product::orderBy($orderByColumn, $orderBy);
        
        if($search_for){
            $data = $data->where(function($q) use ($search_for) {
                $q->where('name_product','like','%'.$search_for.'%');
                $q->orWhere('bar_code','like','%'.$search_for.'%');
            });
        }

        $data = $data->where('client_id', auth()->user()->id_client);
        $data = $data->paginate($perPage);       

        $data->appends(['perPage' => $perPage]);        
        $data->appends(['orderByColumn' => $orderByColumn]);        
        $data->appends(['orderBy' => $orderByReverse]); 
   
        return view('product.index')->with('results',$data)->with('perPage', $perPage)->with('search_for', $search_for);
    }

    public function all_products(Request $request, Product $produc)
    {
        $perPage = $request->input('perPage', 10);
        $search_for = $request->input('search_for');
        $orderByColumn = $request->input('orderByColumn', 'id_product');
        $orderBy = $request->input('orderBy', 'DESC');
        $orderByReverse = $orderBy == 'DESC' ? 'ASC' : 'DESC';

        $data = Product::orderBy($orderByColumn, $orderBy);
        
        if($search_for){
            $data = $data->where('name_product','like','%'.$search_for.'%');
        }
        
        $data = $data->paginate($perPage);       

        $data->appends(['perPage' => $perPage]);        
        $data->appends(['orderByColumn' => $orderByColumn]);        
        $data->appends(['orderBy' => $orderByReverse]); 
   
        return view('product.index')->with('results',$data)->with('perPage', $perPage)->with('search_for', $search_for);
    }

    public function create()
    {   
        $data = new Product;
        return view('product.form')->with('data', $data);
    }

    public function store(Request $request, $id_product = false)
    {   
        $product = Product::firstOrNew(['id_product' => $id_product]);      

        if($id_product)
            $this->authorize('update-product',  $product);

        $request->request->add(['unitary_value' => $this->replaceUnitaryValue($request->unitary_value)]);

        $validator = Validator::make($request->all(), $this->rules($request, $product), $this->messages());

        $redirectErros = $id_product ? route('edit-product',[$id_product]) : route('new-product');

        if ($validator->fails()) {            
            return redirect($redirectErros)
            ->withErrors($validator)
            ->withInput();
        }
        
        $product->name_product = $request->name_product;
        $product->unitary_value = $request->unitary_value;
        $product->client_id = auth()->user()->id_client;
        $product->bar_code = str_pad(auth()->user()->id_client . rand(0,100) . date('dmyhmi'), 20, '0', STR_PAD_LEFT);
        
        $id_product ? $product->update() : $product->save();

        $msg = 'Erro ao salvar dados';
        $type = 'danger';

        if ($product->id_product) {
            $msg = 'Salvo com sucesso.';
            $type = 'success';
        }

        session()->flash('msg', $msg);
        session()->flash('type', $type);

        return redirect()->route('list-product');
    }

    public function edit(Product $product)
    {          
        if(Gate::denies('update-product', $product))
            return redirect(url('/'));

        return view('product.form')
            ->with('data', $product);
    }  

    public function destroy(Request $request)
    {    
        $ids = is_array($request->id_product) ? $request->id_product : [$request->id_product];        
        $product = Product::whereIn('id_product', $ids)->delete();

        $msg = 'Erro ao remover registro';
        $type = 'danger';

        if ($product) {
            $msg = 'Registro(s) removido(s) com sucesso.';
            $type = 'success';
        }

        session()->flash('msg', $msg);
        session()->flash('type', $type);

        return redirect()->back();
    }

    public function rules($request, $product)
    {
        $validate = [
            'name_product' => 'required',
            'unitary_value' => 'required'
        ];

        return $validate;
    }

    public function messages()
    {
        return [
            'name_product.required' => 'Campo obrigatório',            
            'unitary_value.required' => 'Campo obrigatório',            

        ];
    }

    private function replaceUnitaryValue($value)
    {
        return str_replace(',','.', str_replace('.','',$value));
    }
}
