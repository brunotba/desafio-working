<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'id_product';
    protected $fillable = ['id_product', 'unitary_value'];

    public function client()
    {
    	return $this->hasOne('App\Client','id_client','client_id');
    }

}
