<?php

if (!function_exists('generate_link')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function generate_link($arrayAppends = [], $key_alter, $new_value )
    {
    	if(isset($arrayAppends[$key_alter])){
    		$arrayAppends[$key_alter] = $new_value;
    	}

    	$retorno = [];
        foreach ($arrayAppends as $key => $value) {
            $retorno[] = $key . '=' . $value;
        }

        return count($retorno) > 0 ?  implode('&', $retorno) : false;
    }
}
