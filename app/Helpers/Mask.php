<?php

if (!function_exists('maskCPF')) {

	function maskCPF($nbr_cpf) {
		if (!empty($nbr_cpf)) {
			$partes[]     = substr($nbr_cpf, 0, 3);
			$partes[]   = substr($nbr_cpf, 3, 3);
			$partes[]   = substr($nbr_cpf, 6, 3);
			$final = substr($nbr_cpf, 9, 2);
			return  implode('.', $partes) . '-' . $final;
		}
	}

}