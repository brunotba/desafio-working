<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
	use Notifiable;

    protected $primaryKey = 'id_client';
    protected $fillable = ['id_client','name','lastname','cpf','password'];
    protected $hidden = ['password', 'remember_token',];

    /*Retorna todos os produtos com cliente*/
    public function products()
    {
    	return $this->hasMany('App\Product', 'client_id','id_client');
    }
}
