<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{	

	const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  protected $primaryKey = 'id_order';
  protected $fillable = ['status'];
  
  /*retorna quem comprou o produto*/
  public function buyer()
  {
    return $this->hasOne('App\Client', 'id_client', 'client_id');
  }

  /*retorna o produto*/
  public function product()
  { 
    return $this->hasOne('App\Product', 'id_product', 'product_id');
  }

  /*retorna todos os produtos*/
  public function products()
  {
    return $this->hasMany('App\Product','id_product','product_id');
  }

  /*retorna o valor total da compra*/
  public function amount()
  {
    return $this->quantity * $this->product->unitary_value;
  }

  private $array_status = [0 => 'Em aberto', 1 => 'Pago', 2 => 'Cancelado'];

  public function getStatus()
  {
    return $this->array_status[$this->status];
  }

  public function getAllStatus()
  {
    return $this->array_status;
  }
}
