<?php

namespace App\Policies;

use App\Product;
use App\Client;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function updateProduct(Client $client, Product $product)
    {
        return $client->id_client == $product->client_id;  
    }
}
