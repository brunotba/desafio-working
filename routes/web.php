<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::post('authenticate', 'ClientController@authenticate')->name('authenticate');


Route::middleware(['auth'])->group(function () {

	Route::get('/', 'HomeController@index')->name('home');


	Route::prefix('client')->group(function(){

		Route::get('list/{lang?}', 'ClientController@index')->name('list-client');

		Route::get('new', 'ClientController@create')->name('new-client');	
		Route::get('edit/{client}', 'ClientController@edit', function(App\Client $client){
			return $client->id_client;
		})->name('edit-client');
		Route::any('delete/{id_client?}', 'ClientController@destroy')->name('delete-client');
		Route::post('save/{id_client?}', 'ClientController@store')->name('save-update-client');		
		Route::get('logout', 'ClientController@logout')->name('logout');

	});

	Route::prefix('product')->group(function(){
		Route::get('list', 'ProductController@index')->name('list-product');
		Route::get('list-all', 'ProductController@all_products')->name('list-all-products');		
		Route::get('new', 'ProductController@create')->name('new-product');	
		Route::get('edit/{product}', 'ProductController@edit', function(App\Product $product){
			return $product->id_product;
		})->name('edit-product');
		Route::any('delete/{id_product?}', 'ProductController@destroy')->name('delete-product');
		Route::post('save/{id_product?}', 'ProductController@store')->name('save-update-product');		
	});

	Route::prefix('order')->group(function(){
		Route::get('my-shopping', 'OrderController@index')->name('my-shopping');
		Route::get('my-sales', 'OrderController@my_sales')->name('my-sales');
		Route::get('buy/{qty}/{id_product}', 'OrderController@buy_product')->name('buy-product');
		Route::get('alter-status/{id_order}/{status}', function(App\Order $order, $id_order, $status){
			$retorno = $order->find($id_order)->update(['status' => $status]);
			return response()->json(['retorno' => $retorno], 200, [], JSON_PRETTY_PRINT);
		});
	});
	
});




//Route::get('/logout', 'ClientController@logout')->name('logout');
