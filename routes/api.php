<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function(){
	$this->get('product/list', function(App\Product $product){		
		return response()->json($product->all(), 200, [], JSON_PRETTY_PRINT);
	});

	$this->get('product/{id}', function(App\Product $product, $id){		
		return response()->json($product->find($id), 200, [], JSON_PRETTY_PRINT);
	});
});
 

